package com.truckla.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsApplication {

	//Test Dev
	public static void main(String[] args) {
		SpringApplication.run(CarsApplication.class, args);
	}

}
